package com.spring.smartschool187.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.smartschool187.model.BarangModel;
import com.spring.smartschool187.service.BarangService;

@Controller
public class BarangController {

	@Autowired
	private BarangService barangService;

	@RequestMapping(value = "barang")
	public String barang() {
		String jsp = "barang/barang";
		return jsp;
	}

	@RequestMapping(value = "barang/add")
	public String barangAdd(Model model) {
		String kodeBarangGenerator = ""; //lempar backend ke front end
		Long jumlahKode = 0L;
		jumlahKode=this.barangService.countKode() + 1;
		kodeBarangGenerator = "BRG00"+jumlahKode;
		model.addAttribute("kodeBarangGenerator", kodeBarangGenerator); //lempar backend ke front end
		
		String jsp = "barang/add";
		return jsp;
	}

	@RequestMapping(value = "barang/create")
	public String barangCreate(HttpServletRequest request) {
		String kodeBarang = request.getParameter("kodeBarang");
		String namaBarang = request.getParameter("namaBarang");
		BarangModel barangModel = new BarangModel();
		barangModel.setKodeBarang(kodeBarang);
		barangModel.setNamaBarang(namaBarang);
		this.barangService.create(barangModel);
		String jsp = "barang/barang";
		return jsp;
	}

	@RequestMapping(value = "barang/list")
	public String barangList(Model model) {
		List<BarangModel> barangModelList = new ArrayList<BarangModel>();
		barangModelList = this.barangService.searchAll();
		model.addAttribute("barangModelList", barangModelList);
		String jsp = "barang/list";
		return jsp;
	}

	@RequestMapping(value = "barang/detail")
	public String barangDetail(Model model, HttpServletRequest request) {
		String kodeBarang = request.getParameter("kodeBarang");
		BarangModel barangModel = new BarangModel();
		barangModel = this.barangService.searchKode(kodeBarang);
		model.addAttribute("barangModel", barangModel);

		String jsp = "barang/detail";
		return jsp;
	}

	@RequestMapping(value = "barang/edit")
	public String barangEdit(Model model, HttpServletRequest request) {
		String kodeBarang = request.getParameter("kodeBarang");
		BarangModel barangModel = new BarangModel();
		barangModel = this.barangService.searchKode(kodeBarang);
		model.addAttribute("barangModel", barangModel); // backend ke frontend, taro di edit.
		String jsp = "barang/edit";
		return jsp;
	}

	@RequestMapping(value = "barang/remove")
	public String barangRemove(Model model, HttpServletRequest request) {
		String kodeBarang = request.getParameter("kodeBarang");
		BarangModel barangModel = new BarangModel();
		barangModel = this.barangService.searchKode(kodeBarang);
		model.addAttribute("barangModel", barangModel);

		String jsp = "barang/remove";
		return jsp;
	}

	@RequestMapping(value = "barang/update")
	public String barangUpdate(Model model, HttpServletRequest request) {
		String kodeBarang = request.getParameter("kodeBarang");
		String namaBarang = request.getParameter("namaBarang");
		String international = request.getParameter("international");

		BarangModel barangModel = new BarangModel();
		barangModel = this.barangService.searchKode(kodeBarang);
		barangModel.setNamaBarang(namaBarang); // set
		this.barangService.update(barangModel);
		String jsp = "barang/barang";
		return jsp;
	}
	@RequestMapping(value="barang/delete")
	public String barangDelete(Model model, HttpServletRequest request) {
		String kodeBarang = request.getParameter("kodeBarang");
		String namaBarang = request.getParameter("namaBarang");
		
		BarangModel barangModel = new BarangModel();
		barangModel = this.barangService.searchKode(kodeBarang);
		
		this.barangService.delete(barangModel);
		
		String jsp = "barang/barang";
		return jsp;
	}
	
	

}
