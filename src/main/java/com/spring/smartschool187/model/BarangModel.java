package com.spring.smartschool187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_BARANG")

public class BarangModel {
	private String kodeBarang;
	private String namaBarang;
	
	@Id
	@Column(name="KD_BARANG")
	public String getKodeBarang() {
		return kodeBarang;
	}
	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}
	
	@Column(name="NM_BARANG")
	public String getNamaBarang() {
		return namaBarang;
	}
	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}
	
}
