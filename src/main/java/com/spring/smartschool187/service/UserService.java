package com.spring.smartschool187.service;

import com.spring.smartschool187.model.UserModel;

public interface UserService {

	public UserModel searchUsernamePassword(String username, String password);
}
