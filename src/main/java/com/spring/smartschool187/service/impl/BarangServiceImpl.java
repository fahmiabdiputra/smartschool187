package com.spring.smartschool187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.smartschool187.dao.BarangDao;
import com.spring.smartschool187.model.BarangModel;
import com.spring.smartschool187.service.BarangService;

@Service
@Transactional
public class BarangServiceImpl implements BarangService{

	@Autowired
	private BarangDao barangDao;

	@Override
	public void create(BarangModel barangModel) {
		// TODO Auto-generated method stub
		this.barangDao.create(barangModel);
	}

	@Override
	public List<BarangModel> searchAll() {
		// TODO Auto-generated method stub
		return this.barangDao.searchAll();
	}

	@Override
	public BarangModel searchKode(String kodeBarang) {
		// TODO Auto-generated method stub
		return this.barangDao.searchKode(kodeBarang);
	}

	@Override
	public void update(BarangModel barangModel) {
		// TODO Auto-generated method stub
		this.barangDao.update(barangModel);
	}

	@Override
	public void delete(BarangModel barangModel) {
		// TODO Auto-generated method stub
		this.barangDao.delete(barangModel);
	}

	@Override
	public Long countKode() {
		// TODO Auto-generated method stub
		return this.barangDao.countKode();
	}


}
