package com.spring.smartschool187.dao;

import com.spring.smartschool187.model.UserModel;

public interface UserDao {

	public UserModel searchUsernamePassword(String username, String password);
}
