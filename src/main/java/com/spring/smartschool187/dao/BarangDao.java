package com.spring.smartschool187.dao;

import java.util.List;

import com.spring.smartschool187.model.BarangModel;

public interface BarangDao {
	public void create (BarangModel barangModel);

	public List<BarangModel> searchAll();
	public BarangModel searchKode(String kodeBarang);
	public void update(BarangModel barangModel);
	public void delete(BarangModel barangModel);
	public Long countKode();


}
