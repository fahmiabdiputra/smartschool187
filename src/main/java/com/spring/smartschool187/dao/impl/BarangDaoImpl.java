package com.spring.smartschool187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.smartschool187.dao.BarangDao;
import com.spring.smartschool187.model.BarangModel;

@Repository
public class BarangDaoImpl implements BarangDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(BarangModel barangModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(barangModel);
	}

	@Override
	public List<BarangModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<BarangModel> barangModelList = new ArrayList<BarangModel>();
		String query = "from BarangModel";
		String kondisi = " ";
		barangModelList = session.createQuery(query + kondisi).list();
		return barangModelList;
	}

	@Override
	public BarangModel searchKode(String kodeBarang) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); // set start query
		BarangModel barangModel = new BarangModel();
		String query = "from BarangModel";
		String kondisi = " where kodeBarang = '" + kodeBarang + "' "; // query
		barangModel = (BarangModel) session.createQuery(query + kondisi).uniqueResult(); // hql: select from kl di
																								// sql
		return barangModel;
	}

	@Override
	public void update(BarangModel barangModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(barangModel);
	}

	@Override
	public void delete(BarangModel barangModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(barangModel);
	}

	@Override
	public Long countKode() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Long jumlahKode = 0L;
		String query = " select count(kodeBarang) from BarangModel ";
		String kondisi = "";
		jumlahKode = (Long) session.createQuery(query + kondisi).uniqueResult();
		return jumlahKode;
	}

}
