<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Edit Barang</h1>

	<form action="#" method="get" id="form-barang-edit">
		<table>
			<tr>
				<td>Kode Barang</td>
				<td><input type="text" id="kodeBarang" name="kodeBarang" value = "${barangModel.kodeBarang}"/></td>
			</tr>
			<tr>
				<td>Nama Barang</td>
				<td><input type="text" id="namaBarang"name="namaBarang" value = "${barangModel.namaBarang}"/></td>
			</tr>
		
		</table>
		<button type="submit" onclick="validasi();">Simpan</button> 
	</form>
</div>

<script type = "text/javascript">
	function validasi() {
		var kodeBarang = document.getElementById("kodeBarang");
		if (kodeBarang.value == "") {
			kodeBarang.style.borderColor = "red";
		} else {
			kodeBarang.style.borderColor = "gray";
		}
		
		var namaBarang = document.getElementById("namaBarang");
		if (namaBarang.value == "") {
			namaBarang.style.borderColor = "red";
		} else {
			namaBarang.style.borderColor = "gray";
		}
	}
</script>