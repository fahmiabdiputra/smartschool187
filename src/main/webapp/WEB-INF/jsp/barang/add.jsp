<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Tambah Barang</h1>
	<form action="#" method="get" id="form-barang-add">
		<table>
			<tr>
				<td>Kode Barang</td>
				<td><input type="text" id="kodeBarang" name="kodeBarang" value = "${kodeBarangGenerator}" /></td>
			</tr>
			<tr>
				<td>Nama Barang</td>
				<td><input type="text" id="namaBarang" name="namaBarang" /></td>
			</tr>
		</table>
		<button type="submit" onclick="validasi();">Simpan</button>
	</form>
</div>
<script type="text/javascript">
	function validasi() {
		var kodeBarang = document.getElementById("kodeBarang");
		if (kodeBarang.value == "") {
			kodeBarang.style.borderColor = "red";
		} else {
			kodeBarang.style.borderColor = "gray";
		}

		var namaBarang = document.getElementById("namaBarang");
		if (namaBarang.value == "") {
			namaBarang.style.borderColor = "red";
		} else {
			namaBarang.style.borderColor = "gray";
		}

		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-barang-add', function() {
			$.ajax({
				url : 'barang/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {

					$('#modal-input').modal('hide');
					alert("data telah ditambah!");
					loadListBarang();

				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah

	}
</script>