<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Barang</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah
		Barang</button>
	<div>
		<table class="table" id="tbl-genre">
			<tr>
				<td>No</td>
				<td>Kode Barang</td>
				<td>Nama Barang</td>
				<td>Aksi</td>
			</tr>
			<tbody id="list-barang">
			</tbody>
		</table>
	</div>
</div>
<!-- syntax popup -->
<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Barang</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	loadListBarang();

	function loadListBarang() {
		$.ajax({
			url : 'barang/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-barang').html(result);
			}
		});
	}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(function() {

		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'barang/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah

		//fungsi ajax untuk popup lihat
		$('#list-barang').on('click', '#btn-detail', function() {
			var kodeBarang = $(this).val();
			$.ajax({
				url : 'barang/detail.html',
				type : 'get',
				dataType : 'html',
				data : {
					kodeBarang : kodeBarang
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup lihat

		//fungsi ajax untuk popup edit
		$('#list-barang').on('click', '#btn-edit', function() {
			var kodeBarang = $(this).val();
			$.ajax({
				url : 'barang/edit.html',
				type : 'get',
				dataType : 'html',
				data : {
					kodeBarang : kodeBarang
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit

		//fungsi ajax untuk update
		$('#modal-input').on('submit', '#form-barang-edit', function() {

			$.ajax({
				url : 'barang/update.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("data telah diubah!");
					loadListBarang();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update

		//fungsi ajax untuk popup hapus
		$('#list-barang').on('click', '#btn-delete', function() {
			var kodeBarang = $(this).val();
			$.ajax({
				url : 'barang/remove.html',
				type : 'get',
				dataType : 'html',
				data : {
					kodeBarang : kodeBarang
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup hapus

		//fungsi ajax untuk delete
		$('#modal-input').on('submit', '#form-barang-delete', function() {

			$.ajax({
				url : 'barang/delete.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("data telah dihapus!");
					loadListBarang();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete

	});
</script>