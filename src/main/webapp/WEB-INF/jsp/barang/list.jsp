<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${barangModelList}" var="barangModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${barangModel.kodeBarang}</td>
		<td>${barangModel.namaBarang}</td>
		<td>
			<button type="button" class="btn btn-warning" value="${barangModel.kodeBarang}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${barangModel.kodeBarang}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${barangModel.kodeBarang}" id="btn-delete">Hapus</button>
		</td>
	</tr>
</c:forEach>